<?php

namespace App\Entity;

use App\Repository\SubCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SubCategoryRepository::class)
 */
class SubCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="subCategory")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CatalogItem", mappedBy="subCategory")
     */
    private $catalogItems;

    /**
     * SubCategory constructor.
     */
    public function __construct()
    {
        $this->catalogItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category): void
    {
        $this->category = $category;
    }

    public function getCatalogItems()
    {
        return $this->catalogItems;
    }

    public function setCatalogItems($catalogItems): void
    {
        $this->catalogItems = $catalogItems;
    }
}
