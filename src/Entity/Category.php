<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     */
    private $photo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SubCategory", mappedBy="category")
     */
    private $subCategory;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CatalogItem", mappedBy="category")
     */
    private $catalogItems;

    /**
     * Category constructor.
     */
    public function __construct()
    {
        $this->subCategory = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSubCategory()
    {
        return $this->subCategory;
    }

    public function setSubCategory($subCategory): void
    {
        $this->subCategory = $subCategory;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function setPhoto($photo): void
    {
        $this->photo = $photo;
    }

    public function getCatalogItems()
    {
        return $this->catalogItems;
    }

    public function setCatalogItems($catalogItems): void
    {
        $this->catalogItems = $catalogItems;
    }
}
