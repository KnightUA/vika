<?php

namespace App\Controller;

use App\Entity\CatalogItem;
use App\Entity\Category;
use App\Entity\Contact;
use App\Entity\SubCategory;
use App\Form\ContactType;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("", name="main")
     */
    public function index()
    {
        $items = $this->getDoctrine()->getRepository(CatalogItem::class)
            ->findAll();
        return $this->render('main/index.html.twig', [
            'items' => $items,
        ]);
    }

    /**
     * @Route("/catalog", name="catalog")
     */
    public function catalog()
    {
        $items = $this->getDoctrine()->getRepository(CatalogItem::class)
            ->findAll();
        $categories = $this->getDoctrine()->getRepository(Category::class)
            ->findAll();
        return $this->render('catalog/index.html.twig', [
            'items' => $items,
            'category' => $categories
        ]);
    }

    /**
     * @Route("/sub-catalog/{sub_id}/{cat_id}", name="sub_catalog",  defaults={"sub_id": 0, "cat_id": 0})
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param Category|null $category
     * @param SubCategory|null $subCategory
     * @return Response
     */
    public function subCatalog(
        PaginatorInterface $paginator,
        Request $request,
        Category $category = null,
        SubCategory $subCategory = null
    )
    {
        $items = $this->getDoctrine()->getRepository(CatalogItem::class)
            ->findAll();

        if ($request->get('sub_id') != 0) {
            $subCategory = $this->getDoctrine()->getRepository(SubCategory::class)
                ->find($request->get('sub_id'));
        }

        if ($request->get('cat_id') != 0) {
            $category = $this->getDoctrine()->getRepository(Category::class)
                ->find($request->get('cat_id'));
        }

        if ($category && !$subCategory) {
            $items = $this->getDoctrine()->getRepository(CatalogItem::class)
                ->findBy(['category' => $category]);
        }

        if ($category && $subCategory){
            $items = $this->getDoctrine()->getRepository(CatalogItem::class)
                ->findBy(['subCategory' => $subCategory, 'category' => $category]);
        }

        if ($request->get('search')) {
            $search = $request->get('search');
            /** @var QueryBuilder $items */
            $items = $this->getDoctrine()->getRepository(CatalogItem::class)
                ->createQueryBuilder('catalog_item');
            $items->andWhere('catalog_item.title LIKE :val')
                ->setParameter('val', "%$search%")
                ->getQuery();
        }

        $items = $paginator->paginate(
            $items,
            $request->query->getInt('page', 1),
            2 // TODO ITEM PER PAGE
        );

        $categories = $this->getDoctrine()->getRepository(Category::class)
            ->findAll();
        return $this->render('subcatalog/index.html.twig', [
            'items' => $items,
            'category' => $categories,
            'cat' => $category ?? null
        ]);
    }

    /**
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return Response
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, \Swift_Mailer $mailer)
    {
        $items = $this->getDoctrine()->getRepository(CatalogItem::class)
            ->findAll();

        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $message = (new Swift_Message('Інформаційне повідомлення'))
                ->setFrom('work.alekseenko@gmail.com')
                ->setTo($contact->getEmail())
                ->setBody($contact->getDescription());

            $mailer->send($message);


            $this->getDoctrine()->getManager()->persist($contact);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('main');
        }

        return $this->render('contact/index.html.twig', [
            'items' => $items,
            'form' => $form->createView()
        ]);

    }
}
